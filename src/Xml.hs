module Xml
    ( printXml
    )
where

import           System.Environment
import           System.IO
import           Control.Lens
import           Data.Text                     as T



data XmlAttribute = XmlAttribute
    { _attribute_name :: T.Text
    , _attribute_content:: T.Text
    } deriving (Show)
makeLenses ''XmlAttribute


data Xml = Xml
    { _element_name :: T.Text
    , _attributes :: [XmlAttribute]
    , _content :: [Xml]
    } deriving (Show)
makeLenses ''Xml

printXml :: IO ()
printXml = putStrLn $ T.unpack
    (makeXml
        [ Xml
              { _element_name = "svg"
              , _attributes   =
                  [ XmlAttribute { _attribute_name    = "width"
                                 , _attribute_content = "16px"
                                 }
                  , XmlAttribute { _attribute_name    = "height"
                                 , _attribute_content = "16px"
                                 }
                  , XmlAttribute { _attribute_name    = "viewBox"
                                 , _attribute_content = "0 0 16 16"
                                 }
                  ]
              , _content      =
                  [ Xml
                        { _element_name = "g"
                        , _attributes   = []
                        , _content      =
                            [ Xml
                                  { _element_name = "rect"
                                  , _attributes   =
                                      [ XmlAttribute
                                          { _attribute_name    = "style"
                                          , _attribute_content =
                                              "fill:#5830ff;fill-opacity:1;stroke:#000000;stroke-opacity:1"
                                          }
                                      , XmlAttribute { _attribute_name = "width"
                                                     , _attribute_content = "16"
                                                     }
                                      , XmlAttribute
                                          { _attribute_name    = "height"
                                          , _attribute_content = "16"
                                          }
                                      , XmlAttribute { _attribute_name    = "x"
                                                     , _attribute_content = "0"
                                                     }
                                      , XmlAttribute { _attribute_name    = "y"
                                                     , _attribute_content = "0"
                                                     }
                                      ]
                                  , _content      = []
                                  }
                            ]
                        }
                  ]
              }
        ]
    )


makeXml :: [Xml] -> T.Text
makeXml (x : xs) =
    "<"
        <> (x ^. element_name)
        <> " "
        <> makeAttributes (x ^. attributes)
        <> ">"
        <> makeXml (x ^. content)
        <> "</"
        <> (x ^. element_name)
        <> "> "
        <> makeXml xs
makeXml [] = ""

makeAttributes :: [XmlAttribute] -> T.Text
makeAttributes (x : xs) =
    (x ^. attribute_name)
        <> "=\""
        <> (x ^. attribute_content)
        <> "\" "
        <> (makeAttributes xs)
makeAttributes [] = ""
